package com.techu.apitechu.controllers;

import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {
    static final String APIBaseUrl = "/apitechu/v1";
    @GetMapping(APIBaseUrl + "/products")
    public ArrayList<ProductModel> getProducts(){
        System.out.println("getProduts");
        return ApitechuApplication.productModels;
    }
    @GetMapping(APIBaseUrl + "/products/{id}")
    public ProductModel getProductById(@PathVariable String id){
        System.out.println("getProdutById");
        System.out.println("id es" + id);
        ProductModel result = new ProductModel();
        for (ProductModel product:ApitechuApplication.productModels){
            if (product.getId().equals(id)){
                result = product;
            }
        }
        return result;
    }
    @PostMapping (APIBaseUrl + "/products")
    public ProductModel createProduct (@RequestBody ProductModel newProduct){
        System.out.println("createProduct");
        System.out.println("La id del nuevo producto es " + newProduct.getId());
        System.out.println("La desc del nuevo producto es " + newProduct.getDesc());
        System.out.println("El precio del nuevo producto es " + newProduct.getPrice());
        ApitechuApplication.productModels.add(newProduct);
        return newProduct;

    }
    @PutMapping (APIBaseUrl + "/products/{id}")
    public ProductModel updateProduct(@PathVariable String id, @RequestBody ProductModel product){
        System.out.println("updateProduct");
        System.out.println("La id en parámetro de la url es " + id);
        System.out.println("La id del nuevo producto es " + product.getId());
        System.out.println("La desc del nuevo producto es " + product.getDesc());
        System.out.println("El precio del nuevo producto es " + product.getPrice());
        for (ProductModel productInList:ApitechuApplication.productModels){
            if (productInList.getId().equals(id)){
                productInList.setId(product.getId());
                productInList.setDesc(product.getDesc());
                productInList.setPrice(product.getPrice());
            }
        }
        return product;
    }

    @PatchMapping (APIBaseUrl + "/products/{id}")
    public ProductModel updatePartialProduct(@PathVariable String id, @RequestBody ProductModel product){
        System.out.println("updatePartialProduct");
        System.out.println("La id en parámetro de la url es " + id);
        System.out.println("La id del request es " + product.getId());
        System.out.println("La desc a actualizar del producto es " + product.getDesc());
        System.out.println("El precio a actualizar del producto es " + product.getPrice());
        ProductModel result = new ProductModel();
        for (ProductModel productInList:ApitechuApplication.productModels){
            if (productInList.getId().equals(id)){
                if (product.getDesc()!= null)
                    productInList.setDesc(product.getDesc());
                if (product.getPrice() > 0)
                    productInList.setPrice(product.getPrice());
                result = productInList;
            }
        }
        return result;
    }

    @DeleteMapping (APIBaseUrl + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id) {
        System.out.println("deleteProduct");
        System.out.println("La id del producto a borrar es " + id);
        ProductModel result = new ProductModel();
        for (ProductModel product:ApitechuApplication.productModels){
            if (product.getId().equals(id)){
                //ApitechuApplication.productModels.remove(product);
                result = product;
            }
        }
        ApitechuApplication.productModels.remove(result);
        return result;

    }
}
